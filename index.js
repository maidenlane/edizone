const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const { log, ExpressAPILogMiddleware } = require('@rama41222/node-logger');

const config = {
    name: 'edi-zone',
    port: 3000,
    host: '0.0.0.0',
};

const app = express();
const logger = log({ console: true, file: false, label: config.name });

app.use(bodyParser.json());
app.use(cors());
app.use(ExpressAPILogMiddleware(logger, { request: true }));

app.get('/', (req, res) => {
    res.status(200).send('zone-response: ack');
});

app.listen(config.port, config.host, (e)=> {
    if(e) {
        throw new Error('zone-response: nak');
    }
    
    logger.info(`${config.name} running on ${config.host}:${config.port}`);
});
Terms
